<br> 

<h5 class="text-center">Feedback recieved from 'Contact us' page: </h5>
<br>
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Message</th>
    </tr>
  </thead>
  <tbody>

<?php 
$sql = "SELECT * FROM contactus ";
$result = mysqli_query($conn, $sql);

while($row = $result->fetch_assoc()) { ?>

    <tr>
      <th scope="row"> <?php echo $row['name'] ?> </th>
      <td><?php echo $row['email'] ?></td>
      <td><?php echo $row['msg'] ?></td>
    </tr>

<?php } ?>
  </tbody>
</table>
