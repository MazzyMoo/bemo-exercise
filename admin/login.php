<?php 
$TITLE= "Admin Portal";
include_once "./includes/header.php";
include_once "./includes/db.php";
include_once "./includes/functions.php";

if($_SESSION["admin"]==='true') {
  header("Location: dashboard?panel=homepage");
}
else {
  $_SESSION['admin']="";
}

?>

<div class="container">
  <div class="col">	
  	<h1> Welcome to Admin's Portal </h1>
  </div>
<br>
  <div class="col">
  <h5> Please login </h5>
  </div>

  <div class="col">
  <form method="post" action="login">
    <div class="form-group">
      <label for="username">Username</label>
      <input type="text" class="form-control" id="username" name="username" required="">
      <small id="" class="form-text text-muted">Please used the shared credentials.</small>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password" required="">
    </div>
    <button type="submit" class="btn btn-info" id="adminLogin" name="adminLogin">Login</button>
  </form>
  </div>

</div>
<br>

<?php include_once "../includes/footer.php"; ?>
