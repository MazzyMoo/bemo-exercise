<br>
<h5 class="text-center"> Contact Us Panel Settings </h5>

<hr>

<div class="row">
<div class="col">
	<form method="post" action="dashboard" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="contactimage">Upload Image:</label>
	    <input type="file" class="form-control-file" id="contactimage" name="contactimage">
	  </div>
	  <button id="uploadContact" name="uploadContact" type="submit" class="btn btn-outline-info btn-sm" value="uploadContact">Publish Contact Us Image</button>
	</form>
</div>
<div class="col">
		<form method="post" action="dashboard" style="padding-top: 20px; padding-bottom: 30px;">
		<div class="form-group ">
		  	<p class="text-center" style="margin-bottom: 5px;">Meta Title</p>
		    <input type="text" class="form-control" id="contactTitle" name="contactTitle" placeholder="<?php echo $meta_title ?>" >
		  </div>
		  <div class="form-group">
		  	<p class=" text-center" style="margin-bottom: 5px;">Description</p>
		    <textarea class="form-control" id="contactDescription" name="contactDescription" rows="3" placeholder="<?php echo $meta_desc ?>"></textarea>
		  </div>
		<div class="form-check">
		  <input class="form-check-input" type="radio" name="contactRobots" id="exampleRadios1" value=" ">
		  <label class="form-check-label" for="exampleRadios1">
		    Enable Indexing
		  </label>
		</div>
		<div class="form-check">
		  <input class="form-check-input" type="radio" name="contactRobots" id="exampleRadios2" value="no-index" checked="">
		  <label class="form-check-label" for="exampleRadios2">
		    Disable Indexing
		  </label>
		</div>
		<div class="row justify-content-md-center ">
		<div class="col-1">
			<button id="" type="reset" name="" class="btn btn-secondary btn-sm">Clear</button>
		</div>
		<div class="col-1 ">
			<button id="updateContactus" type="submit" name="updateContactus" class="btn btn-secondary btn-sm">Submit</button>
		</div>
		</div>
	</form>
</div>
</div>


<hr>

<div class="row">
	<div class="col">
    <form method="post">
      <textarea id="contactusContent" name="contactusContent"><?php echo $content ?></textarea>
	  <button id="updateContactContent" type="submit" name="updateContactContent" class="btn btn-secondary btn-sm">Submit</button>
    </form>
	</div>
</div>






