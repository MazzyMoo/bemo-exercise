<?php 

$currentFileName = basename($_SERVER['PHP_SELF']);
$dateTimeObject = new DateTime("now", new DateTimeZone("America/Halifax"));
$dateTimeObject->setTimestamp(time()); //adjust the object to correct timestamp
$issueDetail['issueDate'] = $dateTimeObject->format('d.m.Y');
$issueDetail['issueTime'] = $dateTimeObject->format('h:i:sa');
$issueDetail['timeStamp'] = time();

function sanitize ($sanitizeThisThing) {
	$sanitizedThing = trim($sanitizeThisThing);
	$sanitizedThing = stripslashes($sanitizedThing);
	$sanitizedThing = htmlspecialchars($sanitizedThing);

	return $sanitizedThing;
}

if(isset($_POST['updateHomeContent'])) {
	$content = $_POST['homeContent'];
	$sql = "UPDATE pages SET content='$content' WHERE page='home' ";
	$result = mysqli_query($conn, $sql);
	if($result) {
		echo "homepage CONTENT is updated.";
	}
}
if(isset($_POST['updateContactContent'])) {
	$content = $_POST['contactusContent'];
	$sql = "UPDATE pages SET content='$content' WHERE page='contactus' ";
	$result = mysqli_query($conn, $sql);
	if($result) {
		echo "Contact Us CONTENT is updated.";
	}
}

if(isset($_POST['updateHomePage'])) {
	echo "homepage update.";
	$title = sanitize($_POST['homeTitle']);
	$desc = sanitize($_POST['homeDescription']);
	$robots = sanitize($_POST['homeRobots']);

	$sql = "UPDATE pages SET title='$title', description='$desc', robots='$robots' WHERE page='home'";
	$result = mysqli_query($conn, $sql);
	if($result) {
		echo "home page meta data is updated.";
	}
}
if(isset($_POST['updateContactus'])) {
	echo "homepage update.";
	$title = sanitize($_POST['contactTitle']);
	$desc = sanitize($_POST['contactDescription']);
	$robots = sanitize($_POST['contactRobots']);

	$sql = "UPDATE pages SET title='$title', description='$desc', robots='$robots' WHERE page='contactus'";
	$result = mysqli_query($conn, $sql);
	if($result) {
		echo "home page meta data is updated.";
	}
}




if(isset($_POST['uploadBanner'])) {
	$image = sanitize($_FILES["indeximage"]["name"]); 
	$target = "../img/".basename($image);
	$sql = "UPDATE pages SET image='$image' WHERE page='home' ";
	$result = mysqli_query($conn, $sql);
	if($result) {
		if (move_uploaded_file($_FILES['indeximage']['tmp_name'], $target)) {
  			echo "Image uploaded successfully";
  		}
  		else
  			echo "Failed to upload image";
	}
	else 
		echo "error in query";
}
if(isset($_POST['uploadContact'])) {
	$image = sanitize($_FILES["contactimage"]["name"]); 
	$target = "../img/".basename($image);

	$sql = "UPDATE pages SET image='$image' WHERE page='contactus' ";
	$result = mysqli_query($conn, $sql);
	if($result) {
		if (move_uploaded_file($_FILES['contactimage']['tmp_name'], $target)) {
  			echo "Image uploaded successfully";
  		}
  		else
  			echo "Failed to upload image";
	}

}

if(isset($_POST['signOut'])) {
	session_destroy();
	$_SESSION = [];
	header("Location: /index?signedout");
}

if(isset($_POST['adminLogin'])) {
	$usrfield = sanitize($_POST['username']);
	$pwdfield = sanitize($_POST['password']);

	$sql = "SELECT * FROM users WHERE username = '$usrfield' ";
	$result = mysqli_query($conn, $sql);

	if(mysqli_num_rows($result)===1) {
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$hash = $row['password'];

		$_SESSION['admin']="userexist";
		if(crypt($pwdfield, $hash) == $hash){
			$_SESSION['admin']="true";
		  	header("Location: dashboard?panel=homepage");	
		}
	}
	else {
	  	header("Location: ".$currentFileName."?bad");
		$_SESSION['admin']="nouser ".$usrfield ;

		echo "error: please check your credientials.";
	}
}
if(isset($_POST['submitContact'])) {
	$name = sanitize($_POST['name']);
	$email = sanitize($_POST['email']);
	$help = sanitize($_POST['help']);

	$sql = "INSERT INTO contactus(name,email,msg) VALUES ('$name', '$email','$help')";
	if (mysqli_query($conn, $sql)) {
	    echo "New record created successfully";

	    $reciever = "info@bemoacademicconsulting.com";
		$msg = wordwrap($help,70);
		mail($reciever,"\nFeedback: Contact us\n",$msg); //CHANGE MAIL.

	  	header("Location: ".$currentFileName."?submitted");
	}
	else{
	    echo "Error";
	  	header("Location: ".$currentFileName."?error");

	}

}

?>