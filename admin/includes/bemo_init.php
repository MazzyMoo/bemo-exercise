<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.9.0.1
 */

/**
 * Database `bemo`
 */

/* `bemo`.`contactus` */
$contactus = array(
  array('name' => 'potato','email' => 'potato@potato','msg' => 'potato'),
  array('name' => 'final','email' => 'test@test.tst','msg' => 'mock message')
);

/* `bemo`.`pages` */
$pages = array(
  array('id' => '1','page' => 'home','robots' => 'no-index','title' => 'headpage','description' => 'head dsec','keywords' => '','image' => 'obama.jpg','content' => '<p>Hello, World! www.misc.com&nbsp;</p>'),
  array('id' => '2','page' => 'contactus','robots' => 'no-index','title' => 'new contact us','description' => 'robots should be disabled here.','keywords' => '','image' => 'euRabit.jpg','content' => '<h3>BeMo Academic Consulting Inc.</h3>
<p><span style="text-decoration: underline;">Toll-Free:</span> 1-855-900-BeMo (2366)</p>
<p><span style="text-decoration: underline;">Email:</span> info@bemoacademicconsulting.com</p>')
);

/* `bemo`.`users` */
$users = array(
  array('id' => '1','username' => 'bemo','password' => '$2a$07$71aafdddea7ac77b22d12OPH5b/gwbCRGVtQqHGy.vF1Gi7buyXIG'),
  array('id' => '2','username' => 'emad','password' => '$2a$07$1d13de24b05aa10cb0269u9hRXf5rFVFive5AHfniSLCMRLMWdLN6')
);
?>