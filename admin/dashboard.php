<?php 
$TITLE= "Admin's Dashboard";
include_once "./includes/header.php";
include_once "./includes/db.php";
include_once "./includes/functions.php";

if($_SESSION["admin"]!=='true') {
  header("Location: login?badattempt");
}

?>

<div class="container">
	<div class="row" style="padding-top: 10px">	
	<h1 style="padding-bottom: 10px;"> Welcome to Admin's Portal </h1>
	</div>

<div class="row">
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link <?php if(($_GET['panel']==="homepage")) echo "active";?> " href="?panel=homepage">Homepage</a>
  </li>
  <li class="nav-item <?php if(($_GET['panel']==="contactus")) echo "active";?>" >
    <a class="nav-link" href="?panel=contactus">Contact Us</a>
  </li>
  <li class="nav-item <?php if(($_GET['panel']==="tags")) echo "active";?>">
    <a class="nav-link" href="?panel=tags">Global Tags</a>
  </li>
  <li class="nav-item <?php if(($_GET['panel']==="feedback")) echo "active";?>">
    <a class="nav-link" href="?panel=feedback">Feedback</a>
  </li>
</ul>
</div>

<?php 
if(($_GET['panel']==="contactus")) {
	$sql = "SELECT * FROM pages WHERE page='contactus' ";
	$result = mysqli_query($conn, $sql);
	if($row = mysqli_fetch_assoc($result)) {
		$meta_title= $row['title'];
		$meta_desc= $row['description'];
		$content= $row['content'];
	}
	include_once "contactpanel.php";
}
else if(($_GET['panel']==="tags"))
	include_once "tagspanel.php";
else if(($_GET['panel']==="feedback"))
	include_once "feedback.php";
else {
	$sql = "SELECT * FROM pages WHERE page='home' ";
	$result = mysqli_query($conn, $sql);
	if($row = mysqli_fetch_assoc($result)) {
		$meta_title= $row['title'];
		$meta_desc= $row['description'];
		$content= $row['content'];
	}
	include_once "homepanel.php";
}
?>


<div class="d-flex justify-content-center" style="margin: 10px;">
<form method="post">
	<div class="form-group">
     <button type="submit" class="btn btn-danger" id="signOut" name="signOut">Sign out</button>
	</div>
</form>
</div>

</div>



<?php include_once "../includes/footer.php"; ?>

