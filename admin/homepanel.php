<br>
<h5 class="text-center"> Home Page Panel Settings </h5>
<hr>
<div class="row">
<div class="col">
	<form method="post" action="dashboard" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="indeximage">Upload Image:</label>
	    <input type="file" class="form-control-file" id="indeximage" name="indeximage">
	  </div>
      <button id="uploadBanner" name="uploadBanner" type="submit" class="btn btn-outline-info btn-sm" value="uploadBanner">Publish Banner Image</button>
	</form>
</div>
<div class="col">
	<form method="post" action="dashboard" style="padding-bottom: 30px;">
		<div class="form-group ">
		  	<p class="text-center" style="margin-bottom: 5px;">Meta Title</p>
		    <input type="text" class="form-control" id="homeTitle" name="homeTitle" placeholder="<?php echo $meta_title ?>" >
		  </div>
		  <div class="form-group">
		  	<p class=" text-center" style="margin-bottom: 5px;">Description</p>
		    <textarea class="form-control" id="homeDescription" name="homeDescription" rows="3" placeholder="<?php echo $meta_desc ?>"></textarea>
		  </div>

		<div class="form-check">
		  <input class="form-check-input" type="radio" name="homeRobots" id="exampleRadios1" value=" ">
		  <label class="form-check-label" for="exampleRadios1">
		    Enable Indexing
		  </label>
		</div>
		<div class="form-check">
		  <input class="form-check-input" type="radio" name="homeRobots" id="exampleRadios2" value="no-index" checked="">
		  <label class="form-check-label" for="exampleRadios2">
		    Disable Indexing
		  </label>
		</div>
		<div class="row justify-content-md-center ">
		<div class="col-1">
			<button id="" type="reset" name="" class="btn btn-secondary btn-sm">Clear</button>
		</div>
		<div class="col-1 ">
			<button id="updateHomePage" type="submit" name="updateHomePage" class="btn btn-secondary btn-sm">Submit</button>
		</div>
		</div>
	</form>
</div>
</div>
<div class="row">
	<div class="col">
    <form method="post">
      <textarea id="homeContent" name="homeContent"><?php echo $content ?></textarea>
	  <button id="updateHomeContent" type="submit" name="updateHomeContent" class="btn btn-secondary btn-sm " style="margin-top: 5px;">Submit</button>
    </form>
	</div>
</div>

