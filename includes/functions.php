<?php 

	$db_host = "localhost";
	$db_username = "root";
	$db_password = "root";
	$db_name = "bemo";

	$conn = mysqli_connect($db_host, $db_username, $db_password, $db_name);
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}


$currentFileName = basename($_SERVER['PHP_SELF']);
$dateTimeObject = new DateTime("now", new DateTimeZone("America/Halifax"));
$dateTimeObject->setTimestamp(time()); //adjust the object to correct timestamp
$issueDetail['issueDate'] = $dateTimeObject->format('d.m.Y');
$issueDetail['issueTime'] = $dateTimeObject->format('h:i:sa');
$issueDetail['timeStamp'] = time();

function sanitize ($sanitizeThisThing) {
	$sanitizedThing = trim($sanitizeThisThing);
	$sanitizedThing = stripslashes($sanitizedThing);
	$sanitizedThing = htmlspecialchars($sanitizedThing);

	return $sanitizedThing;
}



?>