
<footer class="footer py-5 bg-light text-dark " id="mainFooter">
  <div class="container-fluid">
  	<div class="row">
  		<div class="col">
        <p class="m-0 small">&copy 2013-2016 BeMo Academic Consulting Inc. All rights reserved. <u><a href="http://www.cdainterview.com/disclaimer-privacy-policy.html">Disclaimer & Privacy Policy</a></u> <u><a href="/contactus">Contact Us</a></u></p> 
        <p class="m-0 small"><u><a href="/admin/login">Admin Portal</a></u></p> 
      </div>
  		<div class="p-3 flex-shrink-1 bd-highlight">
        <a href="https://www.facebook.com/bemoacademicconsulting" target="_blank"><img src="/img/facebook.svg"></a>
      </div>
      <div class="p-3 flex-shrink-1 bd-highlight">
        <a href="https://twitter.com/BeMo_AC" target="_blank"><img src="/img/twitter.svg"></a>
  		</div>
  	</div>
  </div>
</footer>

</body>
</html>
