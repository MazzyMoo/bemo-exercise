<?php 
include_once $_SERVER['DOCUMENT_ROOT']."/admin/includes/db.php";

$sql = "SELECT * FROM pages WHERE page='home'";
$result = mysqli_query($conn, $sql);
if($result) {
	$row = mysqli_fetch_assoc($result);
	$TITLE= $row['title'];
	$ROBOTS= $row['robots'];
	$DESCRIPTION= $row['description'];
	$img = $row['image'];
	$content = $row['content'];
}
include_once "includes/header.php";

?>

<div class="bannerimage" style="background-image: url(/img/<?php echo $img ?>);
">
	<div class="mainText">
	<h2>CDN Interview Guide</h2>
	</div>
</div>

<div class="container" style="padding-bottom: 20px;">

<?php echo $content; ?>

</div>

<?php include "includes/footer.php"; ?>