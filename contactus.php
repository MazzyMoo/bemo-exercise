<?php 
$db_host = "localhost";
$db_username = "root";
$db_password = "root";
$db_name = "bemo";

$conn = mysqli_connect($db_host, $db_username, $db_password, $db_name);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT * FROM pages WHERE page='contactus'";
$result = mysqli_query($conn, $sql);
if($result) {
  $row = mysqli_fetch_assoc($result);
  $TITLE= $row['title'];
  $ROBOTS= $row['robots'];
  $DESCRIPTION= $row['description'];
  $CONTENT= $row['content'];
}
include_once "includes/header.php"; 
include_once "includes/functions.php";
?>

<?php 
  $sql = "SELECT image from pages WHERE page='contactus' ";
  $result = mysqli_query($conn, $sql);
  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
  $img = $row['image'];

?>
<div class="contactimage" style="background-image: url(/img/<?php echo $img ?>);
"></div>


<div class="container-fluid" style="padding-left: 50px;">
<div class="row">
    <div class="col">

  <?php 
if(!isset($_GET['submitted'])) {
?>
<?php echo $CONTENT; }
else if(isset($_GET['submitted'])) { ?>
  <h1 class="display-4" style="color: blue;">Thank you. We will get back to you as soon as we can!</h1>
<?php } ?>

  </div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col">
<form method="post" action="contactus" style="padding-top: 20px; padding-bottom: 30px;">
  <div class="form-group ">
  	<p class="text-center" style="margin-bottom: 5px;">Name *</p>
    <input type="text" class="form-control" id="name" name="name" placeholder="" required="">
  </div>
  <div class="form-group">
  	<p class=" text-center" style="margin-bottom: 5px;">Email Address *</p>
    <input type="email" class="form-control" id="email" name="email" placeholder="" required="">
  </div>
  <div class="form-group">
  	<p class=" text-center" style="margin-bottom: 5px;">How Can We Help You? *</p>
    <textarea class="form-control" id="help" name="help" rows="3" required=""></textarea>
  </div>

<div class="d-flex justify-content-center">
	<button id="clearContact" type="reset" name="clearContact" class="btn btn-secondary btn-sm">Clear</button>
	<button id="submitContact" type="submit" name="submitContact" class="btn btn-secondary btn-sm">Submit</button>
</div>

</form>

</div>
</div>
    <small id="" class="form-text text-muted text-center" style="padding-bottom: 10px;">Note: If you are having difficulties with our contact us form above, send us an email to info@bemoacademicconsulting.com (copy & paste the email address)</small>

</div>

<?php include "../includes/footer.php"; ?>